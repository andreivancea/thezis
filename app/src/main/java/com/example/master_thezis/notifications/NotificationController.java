package com.example.master_thezis.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import com.example.master_thezis.R;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;

public class NotificationController {

    public static final String ACTION_APPROVE = "notification.action.approve";
    public static final String ACTION_DENY = "notification.action.deny";
    private static final String CHANNEL_ID = "notification.thezis.channel.id";
    private static final int NOTIFICATION_ID = 132;

    private static NotificationController instance;

    private PendingIntent approvePendingIntent;
    private PendingIntent denyPendingIntent;

    private NotificationManager notificationManager;

    private NotificationResponseListener listener;

    private NotificationController() {
    }

    public static NotificationController getInstance() {
        if (instance == null) {
            instance = new NotificationController();
        }
        return instance;
    }

    public void showNotification(Context context) {
        notificationManager = context.getSystemService(NotificationManager.class);

        createNotificationChannel(context);

        Intent approveIntent = new Intent(context, NotificationActionBroadcastReceiver.class);
        approveIntent.setAction(ACTION_APPROVE);
        approveIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);

        approvePendingIntent = PendingIntent.getBroadcast(context, 0, approveIntent, 0);

        Intent denyIntent = new Intent(context, NotificationActionBroadcastReceiver.class);
        approveIntent.setAction(ACTION_DENY);
        approveIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);

        denyPendingIntent = PendingIntent.getBroadcast(context, 0, denyIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.baseline_assignment_ind_black_18)
                .setContentTitle(context.getString(R.string.check_in_request))
                .setContentText(context.getString(R.string.work_checkin_request_approval))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .addAction(R.drawable.baseline_assignment_ind_black_18, context.getString(R.string.approve), approvePendingIntent)
                .addAction(R.drawable.baseline_assignment_ind_black_18, context.getString(R.string.deny), denyPendingIntent);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public void removeNotification() {
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    private void createNotificationChannel(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void onNotificationResponse(boolean approved) {
        listener.onNotificationResponse(approved);
        removeNotification();
    }

    public void registerNotificationResponseListener(NotificationResponseListener listener) {
        this.listener = listener;
    }

    public interface NotificationResponseListener {
        void onNotificationResponse(boolean approved);
    }
}
