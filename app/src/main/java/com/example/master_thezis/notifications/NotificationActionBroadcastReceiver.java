package com.example.master_thezis.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationActionBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationController.getInstance().onNotificationResponse(NotificationController.ACTION_APPROVE.equals(intent.getAction()));
    }

}
