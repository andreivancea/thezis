package com.example.master_thezis.location;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class GeofenceManager {

    private static final String TAG = GeofenceManager.class.getSimpleName();

    private static final int ONE_MINUTE_MS = 1000 * 60;
    private static final String GEOFENCE_LAT_KEY = "geofence.lat.key";
    private static final String GEOFENCE_LON_KEY = "geofence.lon.key";
    private static final String GEOFENCE_RAD_KEY = "geofence.rad.key";
    private static final String GEOFENCE_LOITERING_DELAY_KEY = "geofence.delay.key";
    private Context context;

    public float lat;
    public float lon;
    public int radius;
    public int loiteringDelay;


    /**
     * Provides access to the Geofencing API.
     */
    private GeofencingClient mGeofencingClient;

    /**
     * The list of geofences used in this sample.
     */
    private ArrayList<Geofence> mGeofenceList;

    /**
     * Used when requesting to add or remove geofences.
     */
    private PendingIntent mGeofencePendingIntent;

    private static GeofenceManager instance;

    private GeofenceManagerCallback callback;

    private GeofenceManager() {
    }

    public static GeofenceManager getInstance() {
        if (instance == null) {
            instance = new GeofenceManager();
        }
        return instance;
    }

    public void init(Context context) {
        this.context = context;

        // Empty list for storing geofences.
        mGeofenceList = new ArrayList<>();

        mGeofencePendingIntent = null;

        mGeofencingClient = LocationServices.getGeofencingClient(context);
        setUpCurrentGeofence();
        addGeofences();
    }

    public void setCallback(GeofenceManagerCallback callback) {
        this.callback = callback;
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    public void setGeofence(float lat, float lon, int radius, int delay) {
        if (!checkPermissions()) {
            if (callback != null) {
                callback.onNeedPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION});
            }
            return;
        }
        saveGeofence(lat, lon, radius, delay);
        setUpCurrentGeofence();
        addGeofences();
    }

    private void setUpCurrentGeofence() {
        mGeofenceList.clear();
        mGeofenceList.add(getLastGeofence());
    }

    /**
     * Adds geofences. This method should be called after the user has granted the location
     * permission.
     */
    @SuppressWarnings("MissingPermission")
    private void addGeofences() {
        Log.w(TAG, "GEOFENCE ADDED");
        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        // Get the status code for the error and log it using a user-friendly message.

                        String errorMessage = GeofenceErrorMessages.getErrorString(context, task.getException());
                        Log.w(TAG, " " + errorMessage);
                    }
                });
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(context, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private Geofence getLastGeofence() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        float lat = prefs.getFloat(GEOFENCE_LAT_KEY, 0f);
        float lon = prefs.getFloat(GEOFENCE_LON_KEY, 0f);
        int rad = prefs.getInt(GEOFENCE_RAD_KEY, 1);
        int delay = prefs.getInt(GEOFENCE_LOITERING_DELAY_KEY, -1);
        return buildGeofence(lat, lon, rad, delay);
    }

    private Geofence buildGeofence(float lat, float lon, int rad, int loiteringDelay) {
        this.lat = lat;
        this.lon = lon;
        this.radius = rad;
        this.loiteringDelay = loiteringDelay;
        return new Geofence.Builder()
                .setRequestId("WORK")
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setCircularRegion(lat, lon, rad)
                .setLoiteringDelay(loiteringDelay <= 0 ? ONE_MINUTE_MS : ONE_MINUTE_MS * loiteringDelay)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_DWELL |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    private void saveGeofence(float lat, float lon, int rad, int delay) {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefs.putFloat(GEOFENCE_LAT_KEY, lat);
        prefs.putFloat(GEOFENCE_LON_KEY, lon);
        prefs.putInt(GEOFENCE_RAD_KEY, rad);
        prefs.putInt(GEOFENCE_LOITERING_DELAY_KEY, delay);
        prefs.apply();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public interface GeofenceManagerCallback {
        void onNeedPermissions(String[] permissions);
    }
}
