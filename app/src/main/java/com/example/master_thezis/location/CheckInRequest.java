package com.example.master_thezis.location;

import com.google.gson.annotations.SerializedName;

public class CheckInRequest {

    @SerializedName("email")
    public String email;

    @SerializedName("name")
    public String displayName;

    @SerializedName("delayTime")
    public long delayTime;

    public CheckInRequest(String email, String displayName, long delayTime) {
        this.email = email;
        this.displayName = displayName;
        this.delayTime = delayTime;
    }
}
