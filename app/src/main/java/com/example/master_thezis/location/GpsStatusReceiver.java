package com.example.master_thezis.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GpsStatusReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED") && GpsStatusProvider.isLocationEnabled(context)) {
            GeofenceManager.getInstance().init(context);
        }
    }
}
