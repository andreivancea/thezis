package com.example.master_thezis.location;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;
import com.example.master_thezis.SessionManager;
import com.example.master_thezis.api.DriveOAuthTokenManager;
import com.example.master_thezis.api.RetrofitEndpointProvider;
import com.example.master_thezis.notifications.NotificationController;
import com.example.master_thezis.util.DelayTimer;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeofenceTransitionsIntentService extends JobIntentService {

    private static final String TAG = "GeofenceService";
    private static final int SERVICE_ID = 12;

    private static boolean checkedIn;
    private static boolean canDoRequest;

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, GeofenceTransitionsIntentService.class, SERVICE_ID, intent);
    }

    @Override
    protected void onHandleWork(@Nullable Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if (geofencingEvent.hasError()) {
            Log.e(TAG, "geofence event has error code: " + geofencingEvent.getErrorCode());
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();


        updateFlags(geofenceTransition);

        if (canDoRequest) {
            if (!checkedIn) {
                NotificationController.getInstance().showNotification(this);
                DelayTimer delayTimer = new DelayTimer();
                delayTimer.startTimer();
                NotificationController.getInstance().registerNotificationResponseListener(approved -> {
                    if (approved) {
                        checkedIn = true;
                        checkInOut(delayTimer.stopTimer());
                    }
                });
            } else {
                checkInOut(0);
                checkedIn = false;
            }
        }
    }

    private void checkInOut(long delayMs) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        long loiteringDelay = GeofenceManager.getInstance().loiteringDelay * 1000 * 60;
        RetrofitEndpointProvider.getDatabaseApi().checkInOut(new CheckInRequest(account.getEmail(), account.getDisplayName(), delayMs + loiteringDelay), DriveOAuthTokenManager.getInstance().getToken())
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.code() == 401) {
                            DriveOAuthTokenManager.getInstance().requestNewToken(token -> {
                                RetrofitEndpointProvider.getDatabaseApi().checkInOut(new CheckInRequest(account.getEmail(), account.getDisplayName(), delayMs + loiteringDelay), DriveOAuthTokenManager.getInstance().getToken())
                                        .enqueue(new Callback<String>() {
                                            @Override
                                            public void onResponse(Call<String> call, Response<String> response) {
                                                if (response.code() == 200) {
                                                    Log.w(TAG, "CheckInCheckOut Successful");
                                                } else {
                                                    Log.w(TAG, "CheckInCheckOut Failed - Error code: " + response.code());
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<String> call, Throwable t) {
                                                Log.w(TAG, "CheckInCheckOut Failed due to: " + t.getMessage());
                                            }
                                        });
                            });
                        } else if (response.code() == 200) {
                            Log.w(TAG, "CheckInCheckOut Successful");
                        } else {
                            Log.w(TAG, "CheckInCheckOut Failed - Error code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.w(TAG, "CheckInCheckOut Failed due to: " + t.getMessage());
                    }
                });
    }

    private void updateFlags(int geofenceTransition) {
        canDoRequest = false;

        Log.w(TAG, getStringGeofenceTransition(geofenceTransition));

        if (SessionManager.isUserLoggedIn(this) && !checkedIn && geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {
            canDoRequest = true;
        }

        if (SessionManager.isUserLoggedIn(this) && checkedIn && geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            canDoRequest = true;
        }
    }

    private String getStringGeofenceTransition(int geofenceTransition) {
        switch (geofenceTransition) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "GEOFENCE TRANSITION ENTER";
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                return "GEOFENCE TRANSITION DWELL";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "GEOFENCE TRANSITION EXIT";
        }
        return null;
    }
}
