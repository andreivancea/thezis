package com.example.master_thezis;

import android.content.Context;
import android.preference.PreferenceManager;

public class SessionManager {
    private static final String IS_LOGGED_IN_KEY = "key.logged";

    public static void setIsUserLoggedIn(Context context, boolean isLoggedIn) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(IS_LOGGED_IN_KEY, isLoggedIn).apply();
    }

    public static boolean isUserLoggedIn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(IS_LOGGED_IN_KEY, false);
    }
}
