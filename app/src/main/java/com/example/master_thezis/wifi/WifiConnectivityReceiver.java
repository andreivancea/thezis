package com.example.master_thezis.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.util.Log;
import com.example.master_thezis.EmployeeCheckingApp;
import com.example.master_thezis.api.CheckInOutRequestQueue;
import com.example.master_thezis.api.DriveOAuthTokenManager;
import com.example.master_thezis.api.RetrofitEndpointProvider;
import com.example.master_thezis.location.CheckInRequest;
import com.example.master_thezis.notifications.NotificationController;
import com.example.master_thezis.util.DelayTimer;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.net.UnknownHostException;

public class WifiConnectivityReceiver extends BroadcastReceiver {

    private static final String TAG = WifiConnectivityReceiver.class.getSimpleName();

    public static final String CONFIGURED_SSID_KEY = "ssid.key";
    private static String lastSSID = "";
    private static boolean isCheckedIn;


    @Override
    public void onReceive(Context context, Intent intent) {

        if (EmployeeCheckingApp.isAppStart) {
            EmployeeCheckingApp.isAppStart = false;
            return;
        }
        int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
        String ssid = null;
        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {
            Log.w(TAG, "Wifi state changed event -> " + translateWifiEventToString(wifiState));

            if (WifiManager.WIFI_STATE_ENABLED == wifiState) {
                Log.w(TAG, "Wifi State Enabled");
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                try {
                    Thread.sleep(6000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ssid = wifiManager.getConnectionInfo().getSSID();
            } else if (WifiManager.WIFI_STATE_DISABLED == wifiState && lastSSID.equals(getConfiguredSSID(context)) && isCheckedIn) {
                Log.w(TAG, "Wifi State Disabled, User was already checked in. Performing checkout request");
                checkInOut(context, 0);
            }
            if (ssid != null) {
                ssid = ssid.replace("\"", "");
                lastSSID = ssid;
                Log.w(TAG, "Wifi Event -> last SSID = " + lastSSID + " || currentSSID = " + ssid);
                if (ssid.equals(getConfiguredSSID(context))) {
                    Log.w(TAG, "Wifi Event -> showing notification!");
                    NotificationController.getInstance().showNotification(context);
                    DelayTimer delayTimer = new DelayTimer();
                    delayTimer.startTimer();
                    NotificationController.getInstance().registerNotificationResponseListener(approved -> {
                        if (approved) {
                            Log.w(TAG, "Wifi Event -> Notification was approved, starting checkin request.");
                            checkInOut(context, delayTimer.stopTimer());
                            isCheckedIn = true;
                        }
                    });
                }
            }
        }
    }

    private String translateWifiEventToString(int wifiState) {
        switch (wifiState) {
            case WifiManager.WIFI_STATE_ENABLING:
                return "Wifi state enabling";
            case WifiManager.WIFI_STATE_ENABLED:
                return "Wifi state ENABLED";
            case WifiManager.WIFI_STATE_DISABLING:
                return "Wifi state disabling";
            case WifiManager.WIFI_STATE_DISABLED:
                return "Wifi state DISABLED";
            case WifiManager.WIFI_STATE_UNKNOWN:
                return "Wifi State UNKNOWN";
        }
        return "";
    }

    private String getConfiguredSSID(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(CONFIGURED_SSID_KEY, null);
    }

    private void checkInOut(Context context, long delayMs) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);
        RetrofitEndpointProvider.getDatabaseApi().checkInOut(new CheckInRequest(account.getEmail(), account.getDisplayName(), delayMs), DriveOAuthTokenManager.getInstance().getToken())
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.code() == 401) {
                            DriveOAuthTokenManager.getInstance().requestNewToken(token -> {
                                RetrofitEndpointProvider.getDatabaseApi().checkInOut(new CheckInRequest(account.getEmail(), account.getDisplayName(), delayMs), DriveOAuthTokenManager.getInstance().getToken())
                                        .enqueue(new Callback<String>() {
                                            @Override
                                            public void onResponse(Call<String> call, Response<String> response) {
                                                if (response.code() == 200) {
                                                    Log.w(TAG, "CheckInCheckOut Successful");
                                                } else {
                                                    Log.w(TAG, "CheckInCheckOut Failed - Error code: " + response.code());
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<String> call, Throwable t) {
                                                if (t instanceof UnknownHostException) {
                                                    CheckInOutRequestQueue.hasToDoCheckinCheckout(delayMs);
                                                }
                                                Log.w(TAG, "CheckInCheckOut Failed due to: " + t.getMessage());
                                            }
                                        });
                            });
                        } else if (response.code() == 200) {
                            Log.w(TAG, "CheckInCheckOut Successful");
                        } else {
                            Log.w(TAG, "CheckInCheckOut Failed - Error code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        if (t instanceof UnknownHostException) {
                            CheckInOutRequestQueue.hasToDoCheckinCheckout(delayMs);
                        }
                        Log.w(TAG, "CheckInCheckOut Failed due to: " + t.getMessage());
                    }
                });
    }

}
