package com.example.master_thezis;

import android.Manifest;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import androidx.annotation.Nullable;
import com.example.master_thezis.api.DriveOAuthTokenManager;
import com.example.master_thezis.api.RetrofitEndpointProvider;
import com.example.master_thezis.auth.GoogleAuthProvider;
import com.example.master_thezis.util.DialogCreator;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;


public class LoginFragment extends BaseFragment {

    private static final int ACCESS_LOCATION_REQUEST_CODE = 234;

    private static final int RC_SIGN_IN = 432;
    private static final String TAG = "LoginFragment";

    private SignInButton signInButton;

    @Override
    protected void configView(View view) {

        setHasOptionsMenu(false);

        signInButton = view.findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);

        signInButton.setOnClickListener(v -> {
            setBusy(true);
            startActivityForResult(GoogleAuthProvider.getInstance().getSignInIntent(), RC_SIGN_IN);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getContext() != null) {

            if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_LOCATION_REQUEST_CODE);
            } else {
                if (DriveOAuthTokenManager.getInstance().getToken() != null && GoogleSignIn.getLastSignedInAccount(getContext()) != null) {
                    navigateToNext();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            AccountManager accountManager = AccountManager.get(getContext());
            accountManager.getAuthToken(account.getAccount(),
                    "oauth2: " + Scopes.DRIVE_FULL,
                    new Bundle(),
                    getActivity(),
                    accountManagerFuture -> {
                        Bundle result;
                        String token;
                        try {
                            result = accountManagerFuture.getResult();

                            Bundle bundle = result;
                            token = bundle.getString(AccountManager.KEY_AUTHTOKEN);

                            DriveOAuthTokenManager.getInstance().storeToken(token);

                            RetrofitEndpointProvider.getDatabaseApi().getUsers(DriveOAuthTokenManager.getInstance().getToken()).enqueue(new Callback<List<String>>() {
                                @Override
                                public void onResponse(@NotNull Call<List<String>> call, @NotNull Response<List<String>> response) {
                                    if (response.code() == 200) {
                                        setBusy(false);
                                        List<String> users = response.body();
                                        boolean found = false;
                                        if (users != null) {
                                            for (String email : users) {
                                                if (email.equals(account.getEmail())) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                navigateToNext();
                                            } else {
                                                onLoginFailed();
                                                DialogCreator.showOkDialog(getContext(), R.string.user_not_found, R.string.we_could_not_find_your_username);
                                            }
                                        } else {
                                            onLoginFailed();
                                            DialogCreator.showOkDialog(getContext(), R.string.something_went_wrong, R.string.server_error);
                                        }
                                    } else {
                                        onLoginFailed();
                                        DialogCreator.showOkDialog(getContext(), R.string.something_went_wrong, R.string.server_error);
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull Call<List<String>> call, @NotNull Throwable t) {
                                    Log.w(TAG, "Login call failed.");
                                    onLoginFailed();
                                    DialogCreator.showOkDialog(getContext(), R.string.something_went_wrong, R.string.server_error);
                                }
                            });
                        } catch (Exception e) {
                            onLoginFailed();
                            e.printStackTrace();
                        }
                    }, new Handler(message -> {
                        System.out.println(message);
                        return false;
                    }));

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            onLoginFailed();
        }
    }

    private void onLoginFailed() {
        GoogleAuthProvider.getInstance().signOut(() -> {

        });
    }

    private void navigateToNext() {
        SessionManager.setIsUserLoggedIn(getContext(), true);
        navController.navigate(R.id.action_loginFragment_to_checknReportFragment);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_login;
    }
}
