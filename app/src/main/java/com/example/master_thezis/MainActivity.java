package com.example.master_thezis;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.example.master_thezis.auth.GoogleAuthProvider;
import com.example.master_thezis.connectivity.ConnectivityReceiver;
import com.example.master_thezis.wifi.WifiConnectivityReceiver;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleAuthProvider.getInstance().init(this);
        registerReceiver(new WifiConnectivityReceiver(), new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        registerReceiver(new ConnectivityReceiver(), intentFilter);
    }
}
