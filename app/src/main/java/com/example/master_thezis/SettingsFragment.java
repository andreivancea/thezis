package com.example.master_thezis;

import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import androidx.annotation.NonNull;
import com.example.master_thezis.auth.GoogleAuthProvider;
import com.example.master_thezis.location.GeofenceManager;

import static com.example.master_thezis.wifi.WifiConnectivityReceiver.CONFIGURED_SSID_KEY;

public class SettingsFragment extends BaseFragment {

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_settings;
    }

    @Override
    protected void configView(View view) {
        setHasOptionsMenu(false);

        EditText etLat = view.findViewById(R.id.et_lat);
        EditText etLon = view.findViewById(R.id.et_lon);
        EditText etRad = view.findViewById(R.id.et_rad);
        EditText etLoiteringDelay = view.findViewById(R.id.et_loitering_delay);
        EditText etWorkSSID = view.findViewById(R.id.et_work_ssid);

        etLat.setText(String.valueOf(GeofenceManager.getInstance().lat));
        etLon.setText(String.valueOf(GeofenceManager.getInstance().lon));
        etRad.setText(String.valueOf(GeofenceManager.getInstance().radius));
        etLoiteringDelay.setText(String.valueOf(GeofenceManager.getInstance().loiteringDelay));
        etWorkSSID.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(CONFIGURED_SSID_KEY, ""));

        view.findViewById(R.id.save_btn).setOnClickListener(v -> {
            GeofenceManager.getInstance().setGeofence(Float.valueOf(etLat.getText().toString()),
                    Float.valueOf(etLon.getText().toString()),
                    Integer.valueOf(etRad.getText().toString()),
                    Integer.valueOf(etLoiteringDelay.getText().toString()));
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString(CONFIGURED_SSID_KEY, etWorkSSID.getText().toString()).apply();
            navController.navigate(R.id.action_settingsFragment_to_loginFragment);
        });
        view.findViewById(R.id.sign_out_btn).setOnClickListener(v -> {
            GoogleAuthProvider.getInstance().signOut(() -> navController.navigate(R.id.action_settingsFragment_to_loginFragment));
            SessionManager.setIsUserLoggedIn(getContext(), false);
        });

    }


}
