package com.example.master_thezis.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.example.master_thezis.R;

public class DialogCreator {

    public static void showOkDialog(Context context, int title, int message) {

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> dialogInterface.dismiss())
                .create();

        alertDialog.show();

    }

    public static void showTwoChoiceDialog(Context context,
                                           int title,
                                           int message,
                                           int positiveBtnText,
                                           int negativeBtnTxt,
                                           DialogInterface.OnClickListener positiveCallback,
                                           DialogInterface.OnClickListener negativeCallback) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveBtnText, positiveCallback)
                .setNegativeButton(negativeBtnTxt, negativeCallback)
                .create();
        alertDialog.show();
    }
}
