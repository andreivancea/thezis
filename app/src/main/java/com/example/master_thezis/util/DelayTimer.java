package com.example.master_thezis.util;

public class DelayTimer {

    private long startTimeMs;

    public void startTimer() {
        startTimeMs = System.currentTimeMillis();
    }

    public long stopTimer() {
        return System.currentTimeMillis() - startTimeMs;
    }

}
