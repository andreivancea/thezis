package com.example.master_thezis.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;

import java.util.ArrayList;
import java.util.List;

public class GoogleAuthProvider {

    private static GoogleAuthProvider instance;
    private static boolean isInitialized;

    private GoogleSignInClient googleSignInClient;
    private Context context;


    private GoogleAuthProvider() {

    }

    public static GoogleAuthProvider getInstance() {
        if (instance == null) {
            instance = new GoogleAuthProvider();
        } else if (!isInitialized) {
            throw new RuntimeException("GoogleAuthProvider must be initialized! Please call GoogleAuthProvider.init(context).");
        }
        return instance;
    }

    public void init(Context context) {
        this.context = context;

        String[] scopes = {
                "openid",
                "https://www.googleapis.com/auth/userinfo.profile",
                "https://www.googleapis.com/auth/drive.scripts",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/drive.readonly",
                "https://www.googleapis.com/auth/spreadsheets.readonly",
                "https://www.googleapis.com/auth/drive.file"
        };

        List<Scope> scopeList = new ArrayList<>();
        for (String scope : scopes) {
            scopeList.add(new Scope(scope));
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.EMAIL), scopeList.toArray(new Scope[0]))
                .requestServerAuthCode("393865285261-kck3jh710ba2sbcb5fpp320r18qbti0h.apps.googleusercontent.com")
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(context, gso);
        isInitialized = true;
    }

    public Intent getSignInIntent() {
        return googleSignInClient.getSignInIntent();
    }

    public void signOut(@NonNull SignOutCallback callback) {
        googleSignInClient.signOut()
                .addOnCompleteListener((Activity) context, task -> {
                    callback.onSignOutComplete();
                });
    }

    public interface SignOutCallback {
        void onSignOutComplete();
    }
}
