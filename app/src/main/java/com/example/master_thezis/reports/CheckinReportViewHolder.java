package com.example.master_thezis.reports;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.master_thezis.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CheckinReportViewHolder extends RecyclerView.ViewHolder {

    private TextView tvDate;
    private TextView tvCheckInTime;
    private TextView tvCheckOutTime;
    private TextView tvHoursWorked;

    public CheckinReportViewHolder(@NonNull View itemView) {
        super(itemView);

        tvDate = itemView.findViewById(R.id.tv_date);
        tvCheckInTime = itemView.findViewById(R.id.tv_checkin_time);
        tvCheckOutTime = itemView.findViewById(R.id.tv_checkout_time);
        tvHoursWorked = itemView.findViewById(R.id.tv_hours_worked);

    }

    public void bind(Report item) {
        SimpleDateFormat responseFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date checkInDate = null;
        Date checkOutDate = null;

        try {
            checkInDate = responseFormat.parse(item.checkInDate.substring(0, item.checkInDate.indexOf(".") - 1));
            checkOutDate = responseFormat.parse(item.checkOutDate.substring(0, item.checkOutDate.indexOf(".") - 1));

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(checkInDate);
            calendar.add(Calendar.HOUR, 3);

            checkInDate = calendar.getTime();

            calendar.setTime(checkOutDate);
            calendar.add(Calendar.HOUR, 3);

            checkOutDate = calendar.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateOnlyFormat = new SimpleDateFormat("dd/MM/yyyy");
        tvDate.setText(dateOnlyFormat.format(checkInDate));

        SimpleDateFormat hourOnlyFormat = new SimpleDateFormat("HH:mm:ss");
        tvCheckInTime.setText(hourOnlyFormat.format(checkInDate));
        tvCheckOutTime.setText(hourOnlyFormat.format(checkOutDate));
        tvHoursWorked.setText(String.format("%.2f", Float.valueOf(item.hoursWorked)));
    }
}
