package com.example.master_thezis.reports;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.master_thezis.R;

import java.util.ArrayList;
import java.util.List;

public class CheckInReportsAdapter extends RecyclerView.Adapter<CheckinReportViewHolder> {

    private List<Report> items = new ArrayList<>();

    @NonNull
    @Override
    public CheckinReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkin_report, parent, false);
        return new CheckinReportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckinReportViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setModel(List<Report> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }
}
