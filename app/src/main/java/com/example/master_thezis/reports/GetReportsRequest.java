package com.example.master_thezis.reports;

import com.google.gson.annotations.SerializedName;

public class GetReportsRequest {

    @SerializedName("route")
    public String route = "GetReports";

    @SerializedName("email")
    public String email;

    public GetReportsRequest(String email) {
        this.email = email;
    }
}
