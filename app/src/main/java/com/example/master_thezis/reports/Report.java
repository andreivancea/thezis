package com.example.master_thezis.reports;

import com.google.gson.annotations.SerializedName;

public class Report {

    @SerializedName("checkIn")
    public String checkInDate;

    @SerializedName("checkOut")
    public String checkOutDate;

    @SerializedName("hoursWorked")
    public String hoursWorked;

    public Report() {
    }

    public Report(String checkInDate, String checkOutDate, String hoursWorked) {
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.hoursWorked = hoursWorked;
    }
}
