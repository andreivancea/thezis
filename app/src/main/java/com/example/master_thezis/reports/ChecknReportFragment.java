package com.example.master_thezis.reports;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.master_thezis.BaseFragment;
import com.example.master_thezis.R;
import com.example.master_thezis.api.DriveOAuthTokenManager;
import com.example.master_thezis.api.RetrofitEndpointProvider;
import com.example.master_thezis.util.DialogCreator;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class ChecknReportFragment extends BaseFragment {

    private RecyclerView rvReports;
    CheckInReportsAdapter adapter;

    @Override
    protected void configView(View view) {

        adapter = new CheckInReportsAdapter();

        rvReports = view.findViewById(R.id.rv_checkin_reports);
        rvReports.setLayoutManager(new LinearLayoutManager(getContext()));
        rvReports.setAdapter(adapter);
        getReportsData();
//        getFakeReportsData();

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.action_map);
        item.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_map) {
            navController.navigate(R.id.action_checknReportFragment_to_viewGeofenceFragment);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getReportsData() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());
        setBusy(true);
        RetrofitEndpointProvider.getDatabaseApi().getReportsForUser(new GetReportsRequest(account.getEmail()), DriveOAuthTokenManager.getInstance().getToken())
                .enqueue(new Callback<List<Report>>() {
                    @Override
                    public void onResponse(Call<List<Report>> call, Response<List<Report>> response) {
                        if (response.code() == 200) {
                            showListOfReports(response.body());
                        } else {
                            DialogCreator.showOkDialog(getContext(), R.string.server_error, R.string.something_went_wrong);
                        }
                        setBusy(false);
                    }

                    @Override
                    public void onFailure(Call<List<Report>> call, Throwable t) {
                        DialogCreator.showOkDialog(getContext(), R.string.server_error, R.string.something_went_wrong);
                        setBusy(false);
                    }
                });
    }

    private void getFakeReportsData() {
        ArrayList<Report> reports = new ArrayList<>();
        reports.add(new Report("21/05/2019 10:15:11", "21/05/2019 19:45:22", "8:25"));
        reports.add(new Report("23/06/2019 8:12:23", "23/06/2019 15:25:12", "6:34"));
        reports.add(new Report("24/06/2019 9:35:53", "24/06/2019 17:21:34", "7:15"));
        reports.add(new Report("24/06/2019 9:35:53", "24/06/2019 17:21:34", "7:15"));
        reports.add(new Report("24/06/2019 9:35:53", "24/06/2019 17:21:34", "7:15"));
        showListOfReports(reports);
    }

    private void showListOfReports(List<Report> items) {
        adapter.setModel(items);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_checkin_report;
    }
}
