package com.example.master_thezis.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitEndpointProvider {

    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    public static DatabaseApi getDatabaseApi() {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new Retrofit.Builder()
                .baseUrl("https://script.google.com/macros/s/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .addInterceptor(logging).build())
                .build()
                .create(DatabaseApi.class);
    }

}
