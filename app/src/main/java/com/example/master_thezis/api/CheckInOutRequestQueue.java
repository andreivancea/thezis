package com.example.master_thezis.api;

import android.content.Context;
import android.util.Log;
import com.example.master_thezis.location.CheckInRequest;
import com.example.master_thezis.util.DelayTimer;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckInOutRequestQueue {

    private static final String TAG = CheckInOutRequestQueue.class.getSimpleName();
    private static boolean needToDoCall = false;
    private static long initialDelay;

    private static DelayTimer delayTimer;

    public static void hasToDoCheckinCheckout(long delay) {
        initialDelay = delay;
        needToDoCall = true;
        delayTimer = new DelayTimer();
        delayTimer.startTimer();
    }

    public static void resumeEnqueuedCalls(Context context) {
        if (needToDoCall) {
            long delayTillNetworkConnectionIsBack = delayTimer.stopTimer();
            long delayMs = initialDelay + delayTillNetworkConnectionIsBack;
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);
            RetrofitEndpointProvider.getDatabaseApi().checkInOut(new CheckInRequest(account.getEmail(), account.getDisplayName(), delayMs), DriveOAuthTokenManager.getInstance().getToken())
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.code() == 401) {
                                DriveOAuthTokenManager.getInstance().requestNewToken(token -> {
                                    RetrofitEndpointProvider.getDatabaseApi().checkInOut(new CheckInRequest(account.getEmail(), account.getDisplayName(), delayMs), DriveOAuthTokenManager.getInstance().getToken())
                                            .enqueue(new Callback<String>() {
                                                @Override
                                                public void onResponse(Call<String> call, Response<String> response) {
                                                    if (response.code() == 200) {
                                                        Log.w(TAG, "CheckInCheckOut Successful");
                                                    } else {
                                                        Log.w(TAG, "CheckInCheckOut Failed - Error code: " + response.code());
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<String> call, Throwable t) {
                                                    Log.w(TAG, "CheckInCheckOut Failed due to: " + t.getMessage());
                                                }
                                            });
                                });
                            } else if (response.code() == 200) {
                                Log.w(TAG, "CheckInCheckOut Successful");
                            } else {
                                Log.w(TAG, "CheckInCheckOut Failed - Error code: " + response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.w(TAG, "CheckInCheckOut Failed due to: " + t.getMessage());
                        }
                    });
            needToDoCall = false;
        }

    }
}
