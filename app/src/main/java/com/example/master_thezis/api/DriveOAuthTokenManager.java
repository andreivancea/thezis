package com.example.master_thezis.api;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.Scopes;

public class DriveOAuthTokenManager {

    private static final String TOKEN_SHARED_PREFS = "token.prefs";
    private static final String TOKEN_KEY = "token.key.google";
    private Context context;

    private static DriveOAuthTokenManager instance;

    private DriveOAuthTokenManager() {

    }

    public static DriveOAuthTokenManager getInstance() {
        if (instance == null) {
            instance = new DriveOAuthTokenManager();
        }
        return instance;
    }

    public void init(Context context) {
        this.context = context;
    }


    public void storeToken(String token) {
        getSharedPrefs().edit().putString(TOKEN_KEY, "OAuth " + token).apply();
    }

    public String getToken() {
        return getSharedPrefs().getString(TOKEN_KEY, null);
    }

    private SharedPreferences getSharedPrefs() {
        return context.getSharedPreferences(TOKEN_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public void requestNewToken(OnNewTokenCallback onNewTokenCallback) {
        AccountManager accountManager = AccountManager.get(context);
        accountManager.getAuthToken(GoogleSignIn.getLastSignedInAccount(context).getAccount(),
                "oauth2: " + Scopes.DRIVE_FULL,
                new Bundle(),
                true,
                accountManagerFuture -> {

                    Bundle result;
                    String token;
                    try {
                        result = accountManagerFuture.getResult();

                        Bundle bundle = result;
                        token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                        storeToken(token);
                        if (onNewTokenCallback != null) {
                            onNewTokenCallback.onNewToken(getToken());
                        }
                    } catch (Exception e) {
                        Log.e("DriveTokenManager", e.getMessage());
                    }

                }
                , new Handler(message -> {
                    System.out.println(message);
                    return false;
                }));
    }

    public interface OnNewTokenCallback {
        public void onNewToken(String token);
    }
}
