package com.example.master_thezis.api;

import com.example.master_thezis.location.CheckInRequest;
import com.example.master_thezis.reports.GetReportsRequest;
import com.example.master_thezis.reports.Report;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface DatabaseApi {

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("AKfycbyUnYIf2Z_BFiTraadl6XLLx0bhecUW_iMOMx8ftth9RZTS1wU/exec")
    Call<List<String>> getUsers(@Header("Authorization") String authToken);


    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("AKfycbyUnYIf2Z_BFiTraadl6XLLx0bhecUW_iMOMx8ftth9RZTS1wU/exec")
    Call<String> checkInOut(@Body CheckInRequest displayName, @Header("Authorization") String authToken);


    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("AKfycbyUnYIf2Z_BFiTraadl6XLLx0bhecUW_iMOMx8ftth9RZTS1wU/exec")
    Call<List<Report>> getReportsForUser(@Body GetReportsRequest reportsRequest, @Header("Authorization") String token);
}
