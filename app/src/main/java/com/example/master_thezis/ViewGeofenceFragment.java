package com.example.master_thezis;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.master_thezis.location.GeofenceManager;
import com.example.master_thezis.location.GpsStatusProvider;
import com.example.master_thezis.util.DialogCreator;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

public class ViewGeofenceFragment extends BaseFragment {

    private static final int ACCESS_LOCATION_REQUEST_CODE = 234;
    private MapView mapView;
    private GoogleMap googleMap;


    @Override
    protected void configView(View view) {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = getView().findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);

        GeofenceManager.getInstance().setCallback(permissions -> requestPermissions(permissions, ACCESS_LOCATION_REQUEST_CODE));

        mapView.getMapAsync(googleMap -> {
            this.googleMap = googleMap;
            googleMap.addCircle(new CircleOptions()
                    .center(new LatLng(GeofenceManager.getInstance().lat, GeofenceManager.getInstance().lon))
                    .radius(GeofenceManager.getInstance().radius)
                    .strokeWidth(0f)
                    .fillColor(0x990000FF));


            if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_LOCATION_REQUEST_CODE);
            } else {
                googleMap.setMyLocationEnabled(true);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        if (getActivity() != null) {
            if (!GpsStatusProvider.isLocationEnabled(getContext())) {
                DialogCreator.showTwoChoiceDialog(getContext(), R.string.turn_on_gps, R.string.your_gps_is_off, R.string.enable, R.string.cancel,
                        (dialogInterface, i) -> {
                            Intent callGPSSettingIntent = new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            getActivity().startActivity(callGPSSettingIntent);
                            dialogInterface.dismiss();
                        },
                        (dialogInterface, i) -> dialogInterface.dismiss());
            } else {
                if (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (googleMap != null && !googleMap.isMyLocationEnabled()) {
                        googleMap.setMyLocationEnabled(true);
                    }
                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_LOCATION_REQUEST_CODE);
                }
            }
        }
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onDestroyView() {
        mapView.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_home;
    }
}
