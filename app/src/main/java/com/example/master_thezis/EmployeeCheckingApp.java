package com.example.master_thezis;

import android.app.Application;
import android.util.Log;
import com.example.master_thezis.api.DriveOAuthTokenManager;
import com.example.master_thezis.location.GeofenceManager;

public class EmployeeCheckingApp extends Application {

    public static boolean isAppStart;

    @Override
    public void onCreate() {
        super.onCreate();
        DriveOAuthTokenManager.getInstance().init(this);
        GeofenceManager.getInstance().init(this);
        Log.w("AAAAAAAAA", "App started");
        isAppStart = true;
    }


}
